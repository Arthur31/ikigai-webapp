import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
// import { AboutComponent } from './about/about.component';
// import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { InvoiceComponent } from './invoice/invoice.component';
// import { AdminComponent } from './admin/admin.component';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { page: 'home' }
  },
  {
    path: 'invoices',
    component: InvoiceComponent,
    data: { page: 'invoice' }
  },
  // {
  //   path: 'about',
  //   component: AboutComponent,
  //   data: { page: 'about' }
  // },
  // {
  //   path: 'contact',
  //   component: ContactComponent,
  //   data: { page: 'contact' }
  // },
  {
    path: 'login',
    component: LoginComponent,
    data: { animation: 'login' }
  },
  // {
  //   path: 'admin',
  //   component: AdminComponent,
  //   data: { animation: 'admin' }
  // },
  {
    path: '**',
    component: Page404Component,
    data: { animation: '404' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
