import { Component, OnInit } from '@angular/core';

import { fadeAnimation, hideMenuAnimation, collapseMenuAnimation } from './animations/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    fadeAnimation,
    hideMenuAnimation,
    collapseMenuAnimation
  ]
})
export class AppComponent implements OnInit {

  constructor() { }
  ngOnInit() {
    if (this.openSide) {
      this.openSide = !this.openSide;
      this.collapse();
    }
  }


  public sidenavState = 'icon';
  public openSide = true;


  public AdminMenu = 'inactive';
  public MentorMenu = 'inactive';
  public collapseMenuToggle = 'inactive';

  sidenavStyle = {
    width: '70px',
  }
  mainStyle = {
    width: 'calc(100% - 70px)',
    left: '70px',
  }


  collapse() {
    this.openSide = !this.openSide;

    if (this.sidenavStyle.width == '70px') {
      // this.cRowStyle.transform = 'translateX(-50px)';
      this.sidenavState = 'text';


      this.sidenavStyle.width = '200px';
      this.mainStyle.left = '200px';
      this.mainStyle.width = 'calc(100% - 200px)';
      // this.titleStyle.width = 'auto';
      // this.titleStyle.margin = 'auto';
      // this.titleStyle.display = 'inline-block';
    } else {
      // this.cRowStyle.transform = 'translateX(10px)';
      this.sidenavState = 'icon';

      this.sidenavStyle.width = '70px';
      this.mainStyle.left = '70px';
      this.mainStyle.width = 'calc(100% - 70px)';
      // this.titleStyle.width = 0;
      // this.titleStyle.margin = 0;
      // this.titleStyle.display = 'none';
    }

  }


  toggleAdminMenu() {
    if (this.openSide) {
      this.AdminMenu = this.AdminMenu === 'active' ? 'inactive' : 'active';
    } else {
      this.AdminMenu = 'inactive';
    }
  }
  toggleMentorMenu() {
    if (this.openSide) {
      this.MentorMenu = this.MentorMenu === 'active' ? 'inactive' : 'active';
    } else {
      this.MentorMenu = 'inactive';
    }

  }
  getPage(outlet) {
    console.log(outlet.activatedRouteData['page'] || 'one');
    return outlet.activatedRouteData['page'] || 'one';
    // return outlet.isActivated ? outlet.activatedRoute : '';
    // return outlet.activatedRouteData.page;
  }
}
